#!/bin/bash

cd build
wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
chmod +x linuxdeploy-x86_64.AppImage
./linuxdeploy-x86_64.AppImage -e lighthouse-control-cli -i ../assets/base-stations.png -d ../assets/lighthouse-control-cli.desktop --appdir AppDir --output appimage
