# Lighthouse Control CLI

A simple CLI utility to control your VR base stations on Linux

## Build instructions

```
git clone https://gitlab.com/lkroll/lighthouse-control-cli.git
cd lighthouse-control-cli
mkdir build
cd build
cmake ..
make
```

## Usage

Either build the program as described above or just grab the Appimage from the Releases.
Then simply mark it as executable and run:

```
chmod +x lighthouse-control-cli
./lighthouse-control-cli
```

Running without any parameters will toggle the state of your lighthouses either on or off depending on their current state.

To list all available parameters just add the -h flag.

To test out if the progam works correctly you can specify the -d flag to start a dry run. This will output what the program would do, but the state of your base stations is not changed.

## License

This program is licensed under the GPLv3, but uses the SimpleBLE library licensed under the MIT license.
