#include <iostream>
#include <string>
#include <vector>

#include <simpleble/SimpleBLE.h>


int get_byte_array(const SimpleBLE::ByteArray& bytes) {
    return ((uint32_t)((uint8_t)bytes.front()));
}

int main(int argc, char **argv) {
    bool dry_run = false;
    bool verbose = false;
    int scan_duration = 1000;

    for (int i = 1; i < argc; i++) {
        if (std::string(argv[i]) == "--help" || std::string(argv[i]) == "-h") {
            std::cout << "Usage: lighthouse-control-cli [option] ... [-t seconds]" << std::endl;
            std::cout << "Options:" << std::endl;
            std::cout << "-h, --help         : Print this help page" << std::endl;
            std::cout << "-d, --dry-run      : Run without changing the power state of your lighthouses" << std::endl;
            std::cout << "-v, --verbose      : Increase verbosity" << std::endl;
            std::cout << "-t, --time seconds : Scan duration in seconds" << std::endl;
            return 0;
        }
        if (std::string(argv[i]) == "--dry-run" || std::string(argv[i]) == "-d") {
            dry_run = true;
            std::cout << "DRY RUN: Lighthouse power states will not change!" << std::endl;
        }
        if (std::string(argv[i]) == "--verbose" || std::string(argv[i]) == "-v") {
            verbose = true;
        }
        if (std::string(argv[i]) == "--time" || std::string(argv[i]) == "-t") {
            scan_duration = std::stoi(argv[i+1]) * 1000;
        }
    }

    SimpleBLE::BluetoothUUID service_uuid = "00001523-1212-efde-1523-785feabcd124";
    SimpleBLE::BluetoothUUID characteristic_uuid = "00001525-1212-efde-1523-785feabcd124";

    if (!SimpleBLE::Adapter::bluetooth_enabled()) {
      std::cout << "Bluetooth is not enabled!" << std::endl;
      return 1;
   }

   auto adapters = SimpleBLE::Adapter::get_adapters();

   if (adapters.empty()) {
      std::cout << "No Bluetooth adapters found!" << std::endl;
      return 1;
   }

   // Use the first adapter
   auto adapter = adapters[0];

   // Bluetooth adapter info
   if (verbose) {
       std::cout << "Adapter identifier: " << adapter.identifier() << std::endl;
       std::cout << "Adapter address: " << adapter.address() << std::endl;
    }

   if (scan_duration == 1000) {
       std::cout << "Scanning for BLE devices..." << std::endl;
   }
   else {
       std::cout << "Scanning for BLE devices... (" << scan_duration / 1000 << "s)" << std::endl;
   }

   adapter.scan_for(scan_duration);

   // Get the list of peripherals found
   std::vector<SimpleBLE::Peripheral> peripherals = adapter.scan_get_results();
   std::vector<SimpleBLE::Peripheral> lighthouses;

   for (auto peripheral : peripherals) {
       if (peripheral.identifier().find("LHB") != std::string::npos) {
           lighthouses.push_back(peripheral);
       }
   }

   if (lighthouses.empty()) {
        std::cout << "No lighthouses found!" << std::endl;
        return 1;
    }
    else {
        bool read = false;
        SimpleBLE::ByteArray rx_data;

        for (auto peripheral : lighthouses) {
            std::cout << "Found Base station: " << peripheral.identifier() << " - " << peripheral.address() << std::endl;

            peripheral.connect();

            // Read first base station characteristic only, to keep state in sync
            if (!read) {
                rx_data = peripheral.read(service_uuid, characteristic_uuid);
                if (verbose) {
                    std::cout << "Characteristic content is: " << get_byte_array(rx_data) << std::endl;
                }
                read = true;
            }

            // if on turn off
            if (get_byte_array(rx_data) == 11) {
                std::cout << "Turning off..." << std::endl;
                if (!dry_run) {
                    peripheral.write_request(service_uuid, characteristic_uuid, "0");
                }
            }

            // if off turn on
            if (get_byte_array(rx_data) == 0) {
                std::cout << "Turning on..." << std::endl;
                if (!dry_run) {
                    peripheral.write_request(service_uuid, characteristic_uuid, "1");
                }
            }

            peripheral.disconnect();
        }
    }

   return 0;
}
